import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


Color whiteColor = Color(0xffffffff);
Color textColor = Color(0xff0D0140);
Color secondaryTextColor = Color(0xff524B6B);
Color buttonColor = Color(0xffE6E1FF);
Color primaryButtonColor = Color(0xff130160);
Color tncButtonColor = Color(0xffFF9228);
Color greyColor = Colors.grey; 

