import 'package:flutter/material.dart';
import 'register.dart';
import 'login.dart';
import 'constans.dart';


class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor: Color(0xffF9F9F9),
      body: Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 32),
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Text("Welcome Back", style:TextStyle(fontSize: 30, fontWeight: FontWeight.bold),), 
          SizedBox(height: 11),
          Text("Please enter your email and password so you can enjoy our services", style:TextStyle(fontSize: 12),
          textAlign: TextAlign.center,
          ),
          SizedBox(height: 64,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("Email", 
              style: TextStyle(fontSize:12, fontWeight: 
              FontWeight.bold 
              ),
              ),
              SizedBox(height: 10),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: TextField(
                 decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Type your email",
                    hintStyle: TextStyle(fontSize:12, color: textColor.withOpacity(0.6)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 17)
                  ),
                ),
              ),
                SizedBox(height: 15),
                Text("Password", 
              style: TextStyle(fontSize:12, fontWeight: 
              FontWeight.bold 
              ),
              ),
              SizedBox(height: 10),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: TextField(
                  obscureText: true,
                 decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Type your password",
                    hintStyle: TextStyle(fontSize:12, color: textColor.withOpacity(0.6)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 17),
                    suffixIcon: Icon(Icons.visibility_off),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    width: 24,
                    height: 24,
                    decoration: BoxDecoration(
                      color: buttonColor,
                      borderRadius: BorderRadiusDirectional.circular(5),
                    )
                  ),
                  SizedBox(width: 15),
                  Text("Remember me", style: TextStyle(fontSize: 12, color: Colors.grey),)
                ],
              ),
              Text("Forgot Password?", style: TextStyle(fontSize: 12, color: Color(0xff0D0140)),),
            ],
          ),
          SizedBox(height: 32,),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 50,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Color(0xff130160),
              ),
              onPressed: (){}, 
              child: Text("LOGIN")
              ),
          ),
           Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20 ),
            height: 50,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                 backgroundColor: Color(0xffD6CDFE),
              ),
              onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage(),),);
              }, 
              child: Text("REGISTER", style: TextStyle(fontSize: 12, color: Color(0xff0D0140),
              fontWeight: FontWeight.bold ),),
              ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Login with another account?",style: TextStyle(fontSize: 12, color: Color(0xff0D0140)),),
              Text("Sign up",style: TextStyle(fontSize: 12, color: Color(0xffFF9228)),
              ),
              
            ],
          ),
        ],
       ),
        ),
      ),
    );
  }
}